/*eslint no-console:0 */

const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const list = require('./routes/list');
const add = require('./routes/add');

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/*
enable CORS for ease of testing
*/
app.use((req,res,next)=>{
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

/*
always return JSON
*/
app.use((req,res,next)=>{
  res.header('Content-Type', 'application/json');
  next();
});

app.use('/list', list);
app.use('/add', add);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res, next) => {

  res.status(err.status || 500);

  console.error(err);

  /*this is just to stay truthful to the assignment*/
  if(err.message === 'ERR_NOT_A_PALINDROME') {
    res.json({status:false});
  }
  else {
    res.json({
      status: false,
      error: err.message
    });
  }

});

module.exports = app;
