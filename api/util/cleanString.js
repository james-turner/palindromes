const cleanString = stringToClean => {

  if(!stringToClean) throw new Error('ERR_UNDEFINED_ARGUMENT');

  return stringToClean.toLowerCase().replace(/[^A-Z0-9]/ig,'');

};

module.exports = cleanString;
