const cleanString = require('./cleanString.js');

const isPalindrome = palindromeCandidate => {

  if(!palindromeCandidate) throw new Error('ERR_UNDEFINED_ARGUMENT');

  const cleanedString = cleanString(palindromeCandidate);

  /*check if the string is the same backwards*/
  return (cleanedString === cleanedString.split('').reverse().join(''));

};

module.exports = isPalindrome;
