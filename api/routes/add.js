const express = require('express');
const router = express.Router();
const Palindromes = require('../model/Palindromes');

router.post('/', (req, res, next) => {

  if(!req.body.palindrome) next(new Error('ERR_UNDEFINED_ARGUMENT'));

  Palindromes.add(req.body.palindrome)
    .then(()=>{
      res.json({status:true});
    })
    .catch(err=>next(err));

});

module.exports = router;
