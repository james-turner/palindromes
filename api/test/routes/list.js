const request = require('supertest');
const app = require('./../../app.js');
const chai = require('chai');

describe('GET /list', function() {
  it('should respond with json', function(done) {
    request(app)
      .get('/list')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        chai.expect(response.body.results).to.exist;
        done();
      })
      .catch(done);
  });
});

describe('GET /list', function() {
  it('should respond a valid API format', function(done) {
    request(app)
      .get('/list')
      .set('Accept', 'application/json')
      .then(response => {
        chai.expect(response.body.results).to.exist;
        done();
      })
      .catch(done);
  });
});
