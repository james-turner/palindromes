const chai = require('chai');
const Palindromes = require('./../../model/Palindromes.js');

describe('model/Palindromes', function() {

  beforeEach(()=>{
    Palindromes.reset();
  });

  it('should return an api-compliant structure',()=>{
    const results = Object.keys(Palindromes.fetch());
    chai.expect(results).to.contain('cursor');
    chai.expect(results).to.contain('list');
  });

  it('should add a palindrome',()=>{

    Palindromes.add('Aba!');

    const results = Palindromes.fetch();
    chai.expect(results.total).to.equal(1);

  });

  it('should not add a wrong palindrome',done=>{

    Palindromes.add('Cba!').catch(err=>{
      done(err==='ERR_NOT_A_PALINDROME');
    });

  });

  it('should not add an empty palindrome',done=>{

    Palindromes.add('').catch(err=>{
      done(err==='ERR_UNDEFINED_ARGUMENT');
    });

  });

  it('should rewrite valid palindromes',done=>{

    Promise.all([
      Palindromes.add('aba'),
      Palindromes.add('kok'),
      Palindromes.add('ywy'),
      Palindromes.add('aba!'),
    ]).then(()=>{
      const results = Palindromes.fetch();
      chai.expect(results.total).to.equal(3);
      done();
    }).catch(done);

  });

  it('should paginate',done=>{

    Promise.all(
      ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o'].map(Palindromes.add)
    ).then(()=>{
      const results = Palindromes.fetch();
      chai.expect(results.total).to.equal(15);
      chai.expect(results.list.length).to.equal(10);
      done();
    }).catch(done);

  });

  it('should go to page 2 & stop paginating',done=>{

    Promise.all(
      ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o'].map(Palindromes.add)
    ).then(()=>{
      const results = Palindromes.fetch();
      const page2 = Palindromes.fetch(results.cursor);
      chai.expect(page2.list[0].palindrome).to.equal('e');
      chai.expect(page2.token).to.equal(undefined);
      done();
    }).catch(done);

  });


});
