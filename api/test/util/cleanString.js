const chai = require('chai');
const cleanString = require('./../../util/cleanString.js');

describe('util/cleanString', function() {

  it('should fail if empty',()=>{

    chai.expect(
      cleanString.bind(this,'')
    ).to.throw('ERR_UNDEFINED_ARGUMENT');

  });

  it('should clean up a verbose string',()=>{

    chai.expect(
      cleanString('A man, a plan, a cat, a ham, a yak, a yam, a hat, a canal-Panama!')
    ).to.equal('amanaplanacatahamayakayamahatacanalpanama');

  });

});
