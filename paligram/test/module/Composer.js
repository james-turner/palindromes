import React from 'react';
import ReactDOM from 'react-dom';
import { mount } from 'enzyme';
import Composer from './../../src/module/Composer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Composer/>, div);
});

it('updates itself', () => {
  const $wrapper = mount(<Composer />);
  $wrapper.find('textarea').get(0).value = 'is it bound';
  $wrapper.find('textarea').simulate('change');
  expect($wrapper.state('palindromeToSubmit')).toEqual('is it bound');
});
