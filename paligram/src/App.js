import React, { Component } from 'react';
import logo from './logo.svg';
import styles from './App.css';
import Transition from 'react-transition-group/Transition';
import TransitionGroup from 'react-transition-group/TransitionGroup';

import {subscribe as messageSubscribe} from './store/messages';
import {subscribe as palindromeSubscribe, fetchFromScratch as palindromeFetchFromScratch, fetchFromCursor as palindromeFetch} from './store/palindromes';

import Palindrome from './module/Palindrome';
import Composer from './module/Composer';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      cursor: undefined,
      palindromes: [],
      messages: [],
    };

    /*
    subscribe to the two global states
    to update app in real time. I totally
    made this pattern up before it grew
    but it's similar to how Redux works.
    On a real more complex app
    I'd probably just got with
    redux in the first place
    */
    palindromeSubscribe(data => {
      this.setState({
        'palindromes': data.palindromes,
        'cursor': data.cursor,
      });
    });

    messageSubscribe(messages => {
      this.setState({
        'messages': messages,
      });
    });

  }

  componentWillMount() {
    /*do an initial fetch*/
    palindromeFetchFromScratch();
  }

  loadMore(ev) {
    ev.preventDefault();
    palindromeFetch();
  }

  /*
  this could use some extra component
  separation, (messages & header at least).
  I tried to strive a balance between solid structured code
  and something you could actually read in a single day.
  */

  render() {
    return (
      <div className={styles.appWithFeedback}>
        <div className={styles.feedback}>
          <TransitionGroup>
            { this.state && this.state.messages && this.state.messages.map((message)=>(
              <Transition timeout={0} key={message.uuid}>
                {(state) => (
                  <div className={styles.feedbackMsg} data-transition-state={state} key={message.uuid}>
                    {message.text}
                  </div>
                )}
              </Transition>
            ))}
          </TransitionGroup>
        </div>
        <div className={styles.app}>
          <header className={styles.header}>
            <img src={logo} alt='Paligram' />
            <Composer />
          </header>
          <div className={styles.palindromes}>
            <TransitionGroup>
              { this.state && this.state.palindromes && this.state.palindromes.map((palindrome,i)=>(
                <Transition timeout={0}  key={palindrome.cursor}>
                  {(state) => (
                    <div className={styles.palyndromeWrap} data-transition-state={state} key={palindrome.cursor}>
                      <Palindrome key={i} date={palindrome.date}>
                        {palindrome.palindrome}
                      </Palindrome>
                    </div>
                  )}
                </Transition>
              ))}
            </TransitionGroup>
            {typeof this.state.cursor !== 'undefined' &&
              <div className={styles.palyndromeWrap} key='plus'>
                <a className={styles.loadMore} onClick={this.loadMore}><span>Load More</span></a>
              </div>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default App;
